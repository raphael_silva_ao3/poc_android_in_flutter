import 'package:flutter/material.dart';
import 'package:poc_android_in_flutter/presentation/presenters/presenter_base.dart';
import 'package:poc_android_in_flutter/presentation/ui/pages/native/native_view_example.dart';

class HomePresenter extends PresenterBase {
  final BuildContext context;
  HomePresenter(this.context);

  void goToNativePlatformViewPage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const NativeViewExample()));
  }
}
