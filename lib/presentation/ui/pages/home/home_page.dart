import 'package:flutter/material.dart';
import 'package:poc_android_in_flutter/presentation/presenters/home/home_presenter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final HomePresenter _presenter;

  @override
  void initState() {
    _presenter = HomePresenter(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('POC Android Inside Flutter')),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: Center(
          child: Column(
            children: [
              const Text("Página Principal", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ElevatedButton(
                        onPressed: () => _presenter.goToNativePlatformViewPage(),
                        child: const Text("Ir para PlatformView nativo"),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
