package com.example.poc_android_in_flutter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.poc_android_in_flutter.flutter_integration.NativeViewFactory;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        flutterEngine
            .getPlatformViewsController()
            .getRegistry()
            .registerViewFactory("hybrid-view-type", new NativeViewFactory(getActivity()));
    }
}
