package com.example.poc_android_in_flutter.views;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.poc_android_in_flutter.R;
import com.example.poc_android_in_flutter.async_task.RaphaAsyncTask;

import java.util.Calendar;

public class ActivityTeste extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);

        setupViews();
    }

    private void setupViews() {
        Button btnIrParaPaginaFlutter = findViewById(R.id.btn_ir_para_pagina_flutter);
        btnIrParaPaginaFlutter.setOnClickListener(v -> {
            Log.d("Clique", "Clicou IR PARA FLUTTER");

            //new GetFCMTokenAndSendToAPIAsyncTask(context, userId).execute();
            //Intent intent = new Intent(this, ActivityTeste.class);
            //startActivity(intent);
        });

        TextView txv_resultado_sync_task_rapha = findViewById(R.id.txv_resultado_sync_task_rapha);
        TextView txv_resultado_porcentagem_sync_task_rapha = findViewById(R.id.txv_resultado_porcentagem_sync_task_rapha);

        Button btnRodarAsyncTask = findViewById(R.id.btn_rodar_async_task);
        btnRodarAsyncTask.setOnClickListener(v -> {
            Log.d("Clique", "Clicou EM RODAR ASYNC TASK");

            new RaphaAsyncTask(Calendar.getInstance().getTime(), this, txv_resultado_sync_task_rapha, txv_resultado_porcentagem_sync_task_rapha).execute();
        });
    }
}