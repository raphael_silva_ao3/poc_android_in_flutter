package com.example.poc_android_in_flutter.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.poc_android_in_flutter.utils.VolleySingleton;

import java.lang.ref.WeakReference;
import java.util.Date;

public class RaphaAsyncTask extends AsyncTask<String, Integer, Void> {
    private Date dataRef;
    private WeakReference<Context> contextRef;
    private TextView txtViewLogRef;
    private TextView txtViewPorcentagemRef;
    private Integer maxRequisicoes = 4;
    private String _textLog = "";

    public RaphaAsyncTask(Date data, Context context, TextView textViewLog, TextView textViewPorcentagem) {
        dataRef = data;
        contextRef = new WeakReference<>(context);
        txtViewLogRef = textViewLog;
        txtViewPorcentagemRef = textViewPorcentagem;
    }

    @Override
    protected Void doInBackground(String... params) {
        for (int i = 1; i <= maxRequisicoes; i++) {
            exibeTextoLog("Efetuando requisição " + i + " de " + maxRequisicoes.toString() + " ...", true);
            sleep(2);
            publishProgress((int) ((i / (float) maxRequisicoes) * 100));
            _efetuarRequisicao(i);
            if (isCancelled()) break;
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        _textLog = "";
        if (txtViewPorcentagemRef != null) txtViewPorcentagemRef.setText("");
        if (txtViewLogRef != null) txtViewLogRef.setText("INCIANDO ...");
    }

    @Override
    protected void onProgressUpdate(Integer... progrss) {
        if (txtViewPorcentagemRef != null) txtViewPorcentagemRef.setText(progrss[0] + " %");
        if (txtViewLogRef != null) txtViewLogRef.setText(_textLog);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (txtViewLogRef != null) txtViewLogRef.setText("FINALIZADO");
    }

    void exibeTextoLog(String texto, Boolean exibeNoTextView) {
        if (exibeNoTextView) {
            _textLog = texto;
            sleep(1);
        }

        Log.d("RAPHA_SYNC_TASK", "  #" + dataRef.toString() + "  ->  " + texto);
    }

    void _efetuarRequisicao(Integer i) {

        String url = "https://www.google.com";

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, url,
                response -> exibeTextoLog("Requisição " + i + ": " + "SUCESSO!", false),
                error -> exibeTextoLog("Requisição " + i + ": " + "ERRO!" + error.getMessage(), false)
        );

        stringRequest.setTag("TesteTag");
        stringRequest.setRetryPolicy(VolleySingleton.getInstance(contextRef.get()).getDefaultRetryPolicyWithoutRetry());
        stringRequest.setShouldCache(false);
        VolleySingleton.getInstance(contextRef.get()).addToRequestQueue(stringRequest);
    }

    void sleep(Integer segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}