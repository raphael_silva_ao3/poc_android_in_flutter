package com.example.poc_android_in_flutter.flutter_integration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.poc_android_in_flutter.R;
import com.example.poc_android_in_flutter.views.ActivityTeste;

import java.util.Map;

import io.flutter.plugin.platform.PlatformView;

class NativeView implements PlatformView {
    @NonNull
    private final View nativeView;

    NativeView(Activity activity, @NonNull Context context, int id, @Nullable Map<String, Object> creationParams) {
        nativeView = LayoutInflater.from(context).inflate(R.layout.layout_native_view, null);
        setupViews(activity, context, nativeView);
    }

    @NonNull
    @Override
    public View getView() {
        return nativeView;
    }

    @Override
    public void dispose() {
    }

    private void setupViews(@NonNull Activity activity, @NonNull Context context, @NonNull View nativeView) {
        TextView textView = nativeView.findViewById(R.id.txv_teste);
        textView.setText("Teste do Rapha");
        textView.setTextColor(Color.GREEN);

        Button btnIrParaPagina2 = nativeView.findViewById(R.id.btn_ir_para_pagina_2);
        btnIrParaPagina2.setOnClickListener(v -> {
            Log.d("Clique", "Clicou 2");

            Intent intent = new Intent(activity, ActivityTeste.class);
            context.startActivity(intent);
        });
    }
}